// resources/assets/js/components/BookOutlist.js
import axios from 'axios'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import html2canvas from 'html2canvas'
import jspdf from 'jspdf'

class BookOutlist extends Component {
  constructor () {
    super()
    this.state = {
      books: []
    }
    this.returnBook = this.returnBook.bind(this)
  }

  componentDidMount () {
    axios.get('/api/bookouts/all', {
      headers: {
        Authorization: JSON.parse(localStorage.getItem('token')),
        'Content-Type': 'application/json'
      }
    }).then(response => {
      this.setState({
        books: response.data.data
      })
    })
    .catch((response) =>{
        if (response.response.status == '401') {
          this.props.history.push('/login')
        }
    })
  }
  exportPDF(){
    const input = document.getElementById('divIdToPrint')
    html2canvas(input)
      .then((canvas) => {
        const imgData = canvas.toDataURL('image/png');    const pdf = new jspdf();
        pdf.addImage(imgData, 'PNG', 0, 0)
        var d = new Date().toISOString().slice(0,10)
        pdf.save("BOOKOUTREPORT"+d+".pdf")
      })
  }
  async returnBook(e){
    let id = e.target.getAttribute('data-id')
    var d = new Date().toISOString().slice(0,10)
    console.log(d)
    await axios.post('/api/book/'+ id +'/return', {
      date_in_actual: d
    }, {
      headers: {
        Authorization: JSON.parse(localStorage.getItem('token')),
        'Content-Type' : 'application/json',
        'Accept' : 'application/json'
      }
    })
    alert("Buku sudah dikembalikan")
    window.location = "/booklist"
  }

  render () {
    const { books } = this.state
    return (
      <div className='container py-4'>
        <div className='row justify-content-center'>
          <div className='col-md-12'>
            <div className='card'>
              <div className='card-header'>All books</div>
              <div className='card-body'>
                <Link className='btn btn-primary btn-sm mb-3' to='/create'>
                  Create new borrow book
                </Link>
                <button className="btn btn-primary btn-sm mb-3 float-right" onClick={this.exportPDF}>
                Export PDF
                </button>
                <table className="table" id="divIdToPrint">
                  <thead>
                    <tr>
                      <th scope="col">Judul Buku</th>
                      <th scope="col">Pengarang</th>
                      <th scope="col">isbn</th>
                      <th scope="col">tahun publikasi</th>
                      <th scope="col">nama peminjam</th>
                      <th scope="col">tanggal pinjam</th>
                      <th scope="col">tanggal kembali aktual</th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                  {
                    this.state.books.map(book => (
                      <tr>
                        <td>{book.book.title}</td>
                        <td>{book.book.author}</td>
                        <td>{book.book.isbn}</td>
                        <td>{book.book.published}</td>
                        <td>{book.member.fullname}</td>
                        <td>{book.date_out}</td>
                        <td>{book.date_in_actual}</td>
                        <td>
                        <button className="btn btn-primary" onClick={this.returnBook}
                            disabled={book.date_in_actual ? true : false} data-id={book.id_books_out}>
                            {book.date_in_actual ? 'Buku ini sudah dikembalikan' : 'Kembalikan'}
                        </button>
                        </td>
                      </tr>
                    ))
                  }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default BookOutlist
