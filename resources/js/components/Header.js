import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => (
  <nav className='navbar navbar-expand-md navbar-light navbar-laravel'>
    <div className='container'>
      <Link className='navbar-brand' to='/book'>Perpustakaan</Link>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className='nav-link' to='/book'>Daftar Buku</Link>
          </li>
          <li className="nav-item">
            <Link className='nav-link' to='/booklist'>Buku yang dipinjam</Link>
          </li>
        </ul>
      </div>
      {
        JSON.parse(localStorage.getItem('token')) ?
        <a className="nav-link" onClick={() => {
          localStorage.removeItem('token')
          window.location = "/login"
        }} href="#" tabIndex="-1" aria-disabled="true">Logout</a> :
        <Link className='nav-link' to='/register'>Register</Link>
      }
    </div>
  </nav>
)

export default Header
