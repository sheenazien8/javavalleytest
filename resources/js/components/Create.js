// resources/assets/js/components/Create.js

import axios from './../helpers/api'
import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'

class Create extends Component {
  constructor () {
    super()
    this.state = {
      date_in: '',
      date_out: '',
      id_member: '',
      id_book: '',
      selectMember: {
        value: ''
      },
      selectBook: {
        value: ''
      },
      books: [],
      members: []
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  async componentDidMount(){
    const member = await axios.get('/api/member',{
      headers: {
        Authorization: JSON.parse(localStorage.getItem('token')),
        'Content-Type': 'application/json'
      }
    })
    const book = await axios.get('/api/books',{
      headers: {
        Authorization: JSON.parse(localStorage.getItem('token')),
        'Content-Type': 'application/json'
      }
    })

    this.setState({
      members: member.data.data,
      books: book.data.data
    })
  }

  async handleSubmit(e){
    e.preventDefault()
    const {data} = await axios.post('/api/book/borrow', this.state, {
      headers: {
        Authorization: JSON.parse(localStorage.getItem('token')),
        'Content-Type' : 'application/json',
        'Accept' : 'application/json'
      }
    })
    this.props.history.push('/booklist')
  }

  handleChange({target}){
    switch (target.name) {
      case 'id_member':
        this.setState({
          selectMember: target.value,
          [target.name]: target.value
        })
        break;
      case 'id_book':
        this.setState({
          selectBook: target.value,
          [target.name]: target.value
        })
        break;
      default:
        this.setState({
          [target.name]: target.value
        })
        break;
    }
  }

  render () {
    return (
      <div className='container py-4'>
        <div className='row justify-content-center'>
          <div className='col-md-8'>
            <div className='card'>
              <div className='card-body'>
                <div>
                  <h1>Create Borrow</h1>
                  <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                      <label>Date In</label>
                      <input type="date" className="form-control" aria-describedby="usernameHelp" name="date_in"
                      onChange={ this.handleChange } placeholder="Enter date In"/>
                    </div>
                    <div className="form-group">
                      <label>Date Out</label>
                      <input type="date" className="form-control" aria-describedby="usernameHelp" name="date_out"
                      onChange={ this.handleChange } placeholder="Enter date Out"/>
                    </div>
                    <div className="form-group">
                      <label>Member</label>
                      <select onChange={ this.handleChange } className="form-control" name="id_member"
                      value={this.state.selectMember.value}>
                        <option value="">-- Pilih Member --</option>
                        {
                          this.state.members.map(member => (
                            <option value={member.id_member} key={member.id_book}>{member.fullname}</option>
                          ))
                        }
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Book</label>
                      <select onChange={ this.handleChange } className="form-control" name="id_book"
                      value={this.state.selectBook.value}>
                      <option value="">-- Pilih Buku --</option>
                        {
                          this.state.books.map(book => (
                            <option value={book.id_book} key={book.id_book}>{book.title + ' - ' + book.author}</option>
                          ))
                        }
                      </select>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Create
