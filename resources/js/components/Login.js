// resources/assets/js/components/Login.js

import axios from './../helpers/api'
import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'

class Login extends Component {
  constructor () {
    super()
    this.state = {
      username: '',
      password: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  async handleSubmit(e){
    e.preventDefault()
    const {data} = await axios.post('/api/auth/login', this.state)
    if (data.data) {
      localStorage.setItem("token", JSON.stringify(data.data.api_token))
      window.location = "/booklist"
    }
  }

  handleChange({target}){
    this.setState({
      [target.name]: target.value
    })
  }

  render () {
    const { projects } = this.state
    return (
      <div className='container py-4'>
        <div className='row justify-content-center'>
          <div className='col-md-8'>
            <div className='card'>
              <div className='card-body'>
                <div>
                  <h1>Login</h1>
                  <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                      <label>Username address</label>
                      <input type="text" className="form-control" id="exampleInputUsername1" aria-describedby="usernameHelp" name="username"
                      onChange={ this.handleChange } placeholder="Enter username"/>
                    </div>
                    <div className="form-group">
                      <label>Password</label>
                      <input type="password" className="form-control" id="exampleInputPassword1" name="password"
                      onChange={ this.handleChange } placeholder="Password"/>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Login
