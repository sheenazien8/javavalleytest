import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'
import Login from './Login'
import Register from './Register'
import BookOutlist from './BookOutlist'
import BookList from './BookList'
import Create from './Create'

class App extends Component {
  render () {
    return (
      <BrowserRouter>
        <div>
          <Header />
          <Switch>
            <Route exact path='/booklist' component={BookOutlist} />
          </Switch>
          <Switch>
            <Route exact path='/book' component={BookList} />
          </Switch>
          <Switch>
            <Route exact path='/login' component={Login} />
          </Switch>
          <Switch>
            <Route exact path='/Register' component={Register} />
          </Switch>
          <Switch>
            <Route exact path='/create' component={Create} />
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}

ReactDOM.render(<App />, document.getElementById('app'))
