import axios from 'axios'
let token
if (JSON.parse(localStorage.getItem('token'))) {
	token = JSON.parse(localStorage.getItem('token'))
}else{
	token = null
}
axios.create({
	baseUrl: 'javavalleytest.deb',
	headers: {
	  Authorization: token,
	  'Content-Type': 'application/json'
	}
})
export default axios
