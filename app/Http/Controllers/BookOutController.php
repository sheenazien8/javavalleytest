<?php

namespace App\Http\Controllers;

use App\Libraries\Auth;
use App\Models\Book;
use App\Models\BookOut;
use App\Models\Member;
use Illuminate\Http\Request;

class BookOutController extends Controller
{
    public function index()
	{
	    $bookOuts = BookOut::paginate();

	    return response()->json([
	        'message' => 'OK',
	        'data' => $bookOuts,
	        'success' => true
	    ]);
	}

    public function all()
	{
	    $bookOuts = BookOut::with('user', 'member', 'book')->get();

	    return response()->json([
	        'message' => 'OK',
	        'data' => $bookOuts,
	        'success' => true
	    ]);
	}

	public function borrow(Request $request)
	{
		$this->validate($request, [
			'date_out' => 'required',
			'date_in' => 'required'
		]);
		$book = Book::find($request->id_book);
		$auth = Auth::user($request);
		$member = Member::find($request->id_member);
		$bookOuts = new BookOut();
		$bookOuts->fill($request->all());
		$bookOuts->member()->associate($member);
		$bookOuts->book()->associate($book);
		$bookOuts->user()->associate($auth);
		$bookOuts->save();


		return response()->json([
			'message' => 'OK',
			'data' => $bookOuts,
			'success' => false
		]);
	}

	public function returnBook(BookOut $bookOuts, Request $request)
	{
		$this->validate($request, [
			'date_in_actual' => 'required'
		]);
		$bookOuts->date_in_actual = $request->date_in_actual;
		$bookOuts->save();


		return response()->json([
			'message' => 'OK',
			'data' => $bookOuts,
			'success' => false
		]);
	}
}
