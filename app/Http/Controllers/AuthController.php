<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:users,username',
            'password' => 'required|min:6'
        ]);
        $request->request->add([
            'password' => bcrypt($request->password),
            'is_active' => 'active'
        ]);
        $user = new User();
        $user->fill($request->all());
        $user->save();

        return response()->json([
            'message' => 'ok',
            'data' => $user,
            'success' => true
        ]);
    }

    public function login(Request $request)
    {
        $carbon = new Carbon();
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $user = User::where('username', $request->username)->first();

        if (!$user) {
            return response()->json([
                'message' => 'username not found',
                'success' => false
            ], 442);
        }else {
            $token = $this->generateToken();
            $checkPassword = Hash::check($request->password, $user->password);
            $expiredToken = $carbon->now()->addHours(1)->format('Y-m-d H:i:s');
            if ($checkPassword) {
                $user->api_token = $token;
                $user->time_expired_token = $expiredToken;
                $user->save();

                return response()->json([
                    'message' => 'OK',
                    'data' => $user,
                    'success' => true
                ]);
            }

        }
    }


    private function generateToken($length = 120)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

}
