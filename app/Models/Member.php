<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'fullname',
		'date_in',
		'date_actual',
		'created',
		'updated'
	];

	public $timestamps = false;
	protected $primaryKey = 'id_member';

	public static function boot()
	{
		parent::boot();

		Member::creating(function($user){
			$user->created = Carbon::now()->format('Y-m-d H:i:s');
		});
	}

}
