<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookOut extends Model
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'date_out',
		'date_in',
		'date_actual',
		'created',
		'updated'
	];

	public $timestamps = false;
	protected $table = 'books_outs';
	protected $primaryKey = 'id_books_out';

	public static function boot()
	{
		parent::boot();

		User::creating(function($user){
			$user->created = Carbon::now()->format('Y-m-d H:i:s');
		});
	}

	public function member()
	{
	    return $this->belongsTo(Member::class, 'id_member');
	}
	public function book()
	{
	    return $this->belongsTo(Book::class, 'id_book');
	}
	public function user()
	{
	    return $this->belongsTo(User::class, 'id_user');
	}

}
