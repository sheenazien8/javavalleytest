<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
		'author',
		'isbn',
		'published',
		'is_active',
		'created',
		'updated'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
	'password',
	];

	public $timestamps = false;
	protected $primaryKey = 'id_book';

	public static function boot()
	{
		parent::boot();

		User::creating(function($user){
			$user->created = Carbon::now()->format('Y-m-d H:i:s');
		});
	}

}
