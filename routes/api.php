<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function() {
	Route::post('/register', 'AuthController@register');
	Route::post('/login', 'AuthController@login');
});

Route::group(['middleware' => 'authenticate'], function() {
    Route::get('bookouts', 'BookOutController@index');
    Route::post('book/borrow', 'BookOutController@borrow');
    Route::post('book/{bookOuts}/return', 'BookOutController@returnBook');
    Route::get('bookouts/all', 'BookOutController@all');

    Route::get('member', function() {
    	return response()->json([
    		'message' => 'OK',
    		'data' => App\Models\Member::all(),
    		'success' => true
    	]);
    });

    Route::get('books', function() {
    	return response()->json([
    		'message' => 'OK',
    		'data' => App\Models\Book::all(),
    		'success' => true
    	]);
    });

    Route::get('borrow', function() {
    	return response()->json([
    		'message' => 'OK',
    		'data' => App\Models\BookOut::with('member', 'user', 'book')->get(),
    		'success' => true
    	]);
    });
});
